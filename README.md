# Arknights Data-Mine

This git contains tools to download and extract assets of the game [Arknights](https://ak.hypergryph.com/index).

The ownership of the assets belongs to [HyperGryph](https://www.hypergryph.com/#/).


## Scripts

### Requirements
```cmd
pip install UnityPy
```

### EXTRACT.py
* extracts all images from the downloaded raw assets to extracted.

### POST_FUSE_ALPHA.py
* merges the the alpha channel image into the original images
* has to be executed after extract.py



