from UnityPy import AssetsManager
import os

PATH = os.path.dirname(os.path.realpath(__file__))
src = os.path.join(PATH,'assets')
dst = os.path.join(PATH,'extracted')

def export(obj, path = None, parent_dir = None):
   data = obj.read()
   if not path:
      path = os.path.join(parent_dir, data.name)
   print(path)
   os.makedirs(os.path.dirname(path), exist_ok=True)
   typ = getattr(obj, 'type', None)
   if typ in ['Sprite','Texture2D']:
      try:
         if parent_dir:
            path += '.png'
         elif path[-4] == '.':
            path = path[:-4]+'.png'
         if obj.type == 'Texture2D':
            if os.path.exists(path):
               return        
         data.image.save(path)
      except EOFError:
         pass
   elif typ == "TextAsset":
      with open(path, 'wb') as f:
         f.write(data.script)
   else:
      pass
      #print(obj.type, dfp)
   

for root, dirs, files in os.walk(src, topdown=False):
   for fp in files:
      fp = os.path.realpath(os.path.join(root, fp))
      b = AssetsManager()
      b.load_file(fp)
      for asset in b.assets.values():
         if not asset.container:
            continue
         exported = False
         # list container items first
         for ap, obj in asset.container.items():
            if getattr(obj, 'type', '') in ['Sprite','Texture2D','TextAsset']:
               export(obj, os.path.join(dst, *ap.split('/')))
               exported = True
         if not exported:
            for obj in asset.objects.values():
               if getattr(obj, 'type', '') in ['Sprite','Texture2D','TextAsset']:
                  export(obj, None,fp.replace('assets','extracted').replace('.unity3d',''))

            