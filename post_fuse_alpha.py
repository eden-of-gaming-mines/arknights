from PIL import Image
import os
import re

PATH = os.path.dirname(os.path.realpath(__file__))
dst = os.path.join(PATH,'extracted')

reAlpha = re.compile('(.+)\[alpha\].png')
for root, dirs, files in os.walk(dst, topdown=False):
   for fp in files:
       if reAlpha.match(fp):
           print(fp)
           ap = os.path.realpath(os.path.join(root, fp))
           fp = os.path.realpath(os.path.join(root, ''.join(fp.partition('[alpha]')[::2])))
           alpha = Image.open(ap)
           image = Image.open(fp)
           image = Image.merge('RGBA', (*image.split()[:3], alpha.split()[0]))
           image.save(fp)
           os.unlink(ap)